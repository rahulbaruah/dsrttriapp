@extends('frontend_layouts.master')

@section('title', 'Notice | DSRTTRI')


@section('content')
<div class="kingster-page-wrapper" id="kingster-page-wrapper" style="padding-top:30px;padding-bottom:30px">
   
    <div class="container">

    <div class="row">

      <!-- Post Content Column -->
      <div class="col-lg-8">

        <!-- Title -->
        <h1 class="mt-4">{{ $notice->subject }}</h1>
        <!-- Date/Time -->
        <p><small>Posted on {{ \Carbon\Carbon::parse($notice->created_at)->format('d-m-yy g:ia') }}</small></p>

        <hr>
        
        <p>{{ $notice->description }}</p>
        
        @if($notice->uploaded_file)
        <!--<hr>-->
        <p><u>Downloads</u></p>
        <a href="{{ asset($notice->uploaded_file) }}" target="_blank">{{ basename($notice->uploaded_file) }}</a>
        @endif
      </div>

    </div>
    <!-- /.row -->

  </div>
                    
</div>
@endsection