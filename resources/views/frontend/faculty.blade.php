@extends('frontend_layouts.master')

@section('title', 'Faculties | DSRTTRI')


@section('content')
<div class="kingster-page-wrapper" id="kingster-page-wrapper" style="padding-top:30px">
    <div class="container">
                    
    <div class="kingster-item-pdlr">
        <h4 style="margin-bottom:0;">Faculties</h4>
    </div>
    
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-white" style="padding-top:0;padding-left: 0;margin-left: 20px;">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Faculties</li>
      </ol>
    </nav>
    <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
                    
    <div class=" kingster-sidebar-wrap clearfix kingster-line-height-0 kingster-sidebar-style-right">
                        
                        <div class=" kingster-sidebar-center kingster-column-100 kingster-line-height">
                            <div class="gdlr-core-page-builder-body">
                                <div class="gdlr-core-pbf-wrapper" style="padding-top:0">
                                    <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                                        <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
        <div>


                                        <p class="">The College has a qualified, research-oriented and trained team of faculty members. The faculty members are committed to provide quality education to the students and thus serve the society by inculcating the true values of life with education. The College regularly organizes special lectures, seminars and workshops by guest speakers and faculty from renowned educational institutes from within the state.</p>
                                        </div>

                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" id="div_1dd7_19">
                                                    <div class="gdlr-core-text-box-item-content" id="div_1dd7_20">
                                                        <div>
                                                        <img src="/frontend/images/faculty1.jpg" alt="Principal Image" style="float:left;padding: 0 1em 1em 0;"/>
                                                        <img src="/frontend/images/faculty2.jpg" alt="Principal Image" style="float:left;padding: 0 1em 1em 0;"/>
                                                        <img src="/frontend/images/faculty3.jpg" alt="Principal Image" style="float:left;padding: 0 1em 1em 0;"/>
                                                        <!--<img src="/frontend/images/faculty4.jpg" alt="Principal Image" style="float:left;padding: 0 1em 1em 0;"/>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>





                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!--<div class=" kingster-sidebar-right kingster-column-20 kingster-line-height kingster-line-height">
                            <div class="kingster-sidebar-area kingster-item-pdlr" style="padding-top:0">
                                
                                <div id="text-18" class="widget widget_text kingster-widget">
                                    <div class="textwidget">
                                        <div class="gdlr-core-widget-box-shortcode " style="color: #ffffff ;padding: 30px 45px;background-color: #ec2f45 ;">
                                            <div class="gdlr-core-widget-box-shortcode-content">
                                                </p>
                                                <h3 style="font-size: 20px; color: #fff; margin-bottom: 25px;">Contact Info</h3>
                                                <p><span style="font-size: 15px;">Chamata
                                                    <br /> <br /> Nalbari - 781306</span></p>
                                                <p><span style="font-size: 15px;"><i class="fa fa-phone" aria-hidden="true"></i> (03624) 232000 <br /><i class="fa fa-envelope-o" aria-hidden="true"></i> info@dsrttri.com<br /> </span></p>
                                                <p><span style="font-size: 16px;"><i class="fa fa-clock-o" aria-hidden="true"></i> Mon &#8211; Sat 9:00A.M. &#8211; 5:00P.M.</span></p> <span class="gdlr-core-space-shortcode" style="margin-top: 40px ;"></span>
                                                
                                                <p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>-->
                        
                    </div>    
                
    </div>
                    
</div>
@endsection