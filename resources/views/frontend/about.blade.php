@extends('frontend_layouts.master')

@section('title', 'About us | DSRTTRI')


@section('content')
<div class="kingster-page-wrapper" id="kingster-page-wrapper" style="padding-top:30px">
    <div class="container">
                    
    <div class="kingster-item-pdlr">
        <h4 style="margin-bottom:0;">About DSRTTRI</h4>
    </div>
    
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-white" style="padding-top:0;padding-left: 0;margin-left: 20px;">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">About Us</li>
      </ol>
    </nav>
    <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
    <div class="row">
        <!--<div class="col-md-4">
            <img src="https://via.placeholder.com/450x360/000000/ffffff"></img>
            
            
            
            <div class="left-sub-box">
                <img src="https://via.placeholder.com/450x530/ec2f45/ffffff"></img>
            </div>
        </div>-->
        <div class="col-md-12">
            <!--<ul class="right-box-list">
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff" class="rounded-circle"/></div><div class="col-md-8 col-7">Ranked amongst the Prominent B – Schools of INDIA by GHRDC – CSR B School Survey 2019</div></div></li>
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff" class="rounded-circle"></img></div><div class="col-md-8 col-7">Ranked as the Times Best Professional Institute of North East India in Terms of Quality Training & Placements by Times of India – Business Awards 2019</div></div></li>
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff" class="rounded-circle"></img></div><div class="col-md-8 col-7">Jettwings Business School – (JBS)   is affiliated under the prestigious Government of Assam State University - Gauhati University (NAAC RATING 5 STAR) & Approved by Department of Higher Education, Government of ASSAM</div></div></li>
            </ul>-->
            
            <div class="right-sub-box">
            <!--<img src="https://via.placeholder.com/900x280/000000/ffffff"></img>-->
            
              <h3 class="font-weight-600 mt-0 font-28 line-bottom">Dr. Sarvepolli Radhakrishnan Teacher's Training and Research Institute</h3>
              <p class="" align="justify">Project Chamata Educational Society, Chamata was established in the year 2010. The real dream of this society is to make the greater Pachim Nalbari area a hundred percent educated and to develop it as a hub of socio-cultural area.
              <p align="justify">With a view to make this dream real Project Chamata Educational Society decided to start a B.Ed college at Chamata. In the year 2011 it applied to NCTE for recognition of Dr. Sarvepolli Radhakrishnan Teachers’ Training and Research Institute.</p>
              <p align="justify">In the year 2013 on 2nd October NCTE’s visiting team comprising Prof. DN Singh form Patna University and Prof. T. Jha from Jharkhand University visited the college and expressed their satisfaction regarding its assets and infrastructural facilities. In the meantime Assam Govt. issued No objection certificate and Gauhati University issued temporary affiliation to the institute.</p>
              <p align="justify">After long six years incessant endeavor Dr. Sarvepolli Radhakrishnan Teachers’ Training and Research Institute received its recognition from NCTE and permanent affiliation from Gauhati university in the year 2017 and started its first batch of admission on 11th September 2017.</p>
              <p align="justify">The enlightened group who dedicated their whole hearted attempt to establish a B.Ed College at Chamata felt enormous joy and happiness for the success. </p>
              <p align="justify">Located at a rural area about 10 K.M. west of Nalbari District headquarter, Chamata is well connected with road. Regular bus services are available from Guwahati, Nalbari and Barpeta. The nearest Railway station is Nalbari. The college has admitted 50 trainees in the session 2017-18 as per the directives of Gauhati University and NCTE. Adequate infrastructural facilities and well qualified teaching staff is one of the points of attraction of this institute. The Management of this institute is constituted with eminent personalities who always strive for providing best quality of teacher education through Dr. Sarvepolli Radhakrishnan Teachers’ Training and Research Institute.</p>
            </div>
            
            <!--<div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
                <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #000000; ;border-bottom-width: 1px ;"></div>
            </div>-->
        </div>
    </div>

    </div>
</div>
@endsection

@section('script')
    @parent
<script type="text/javascript">
    document.getElementById("rdMoreBtn").addEventListener("click", function(event){
        event.preventDefault();
        readMoreFunction();
    });
    
    document.getElementById("rdLessBtn").addEventListener("click", function(event){
        event.preventDefault();
        readMoreFunction();
    });
    
    function readMoreFunction() {
        var moreText = document.getElementById("more");
        var rdMoreBtn = document.getElementById("rdMoreBtn");
        if (moreText.style.display === "inline") {
            rdMoreBtn.style.display = "inline";
            moreText.style.display = "none";
          } else {
            rdMoreBtn.style.display = "none";
            moreText.style.display = "inline";
          }
    }
</script>
@endsection