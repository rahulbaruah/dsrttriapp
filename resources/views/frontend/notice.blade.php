@extends('frontend_layouts.master')

@section('title', ucwords($category).' Notice | DSRTTRI')


@section('content')
<div class="kingster-page-wrapper" id="kingster-page-wrapper" style="padding-top:30px;padding-bottom:30px">
    <div class="container">
                    
    <div class="kingster-item-pdlr">
        <h4 style="margin-bottom:0;">{{ ucwords($category) }} Notice</h4>
    </div>
    
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-white" style="padding-top:0;padding-left: 0;margin-left: 20px;">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{ ucwords($category) }} Notice</li>
      </ol>
    </nav>
    <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
                    
    <div class=" kingster-sidebar-wrap clearfix kingster-sidebar-style-right">
        @foreach($notices as $notice)
        <div class="card mb-4">
          <div class="card-body">
            <h4 class="card-title">{{ $notice->subject }}</h4>
            <!--<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>-->
            <a href="/noticedetail/{{ $notice->id }}">Read More &rarr;</a>
          </div>
          <div class="card-footer text-muted">
            Posted on {{ \Carbon\Carbon::parse($notice->created_at)->format('d-m-yy g:ia') }}
          </div>
        </div>
        @endforeach
    </div>       
                
    </div>
                    
</div>
@endsection