@extends('frontend_layouts.master')

@section('title', 'Contact us | DSRTTRI')


@section('content')
<div class="kingster-page-wrapper" id="kingster-page-wrapper" style="padding-top:30px;padding-bottom:30px">
    <div class="container">
                    
    <div class="kingster-item-pdlr">
        <h4 style="margin-bottom:0;">Contact Us</h4>
    </div>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-white" style="padding-top:0;padding-left: 0;margin-left: 20px;">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
      </ol>
    </nav>
    <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
    
    <address><strong>Address:</strong> Chamata, Nalbari, Assam - 781306
    <br/>
    <strong>Phone:</strong> <a href="tel:03624232000">(03624) 232000</a>
    </address>
    <div class="clearfix"></div>
    <br/>
    <h4>Enquiry Form</h4>
    <form id="contact-form" action="/script/contact.php" method="POST">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputName">Name</label>
                                    <input id="name" type="text" class="form-control" id="exampleInputName" placeholder="Name" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputPhone">Phone</label>
                                    <input id="phone" type="text" class="form-control" id="exampleInputPhone" placeholder="Phone" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email</label>
                                    <input id="email" type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Message</label>
                            <textarea id="message" class="form-control" rows="3" required></textarea>
                        </div>
                        
                        <button id="submit_btn" type="submit" class="btn tf-btn btn-primary">Submit</button>
                    </form>
</div>
@endsection

@section('script')
    @parent
<script type="text/javascript">
    window.addEventListener('DOMContentLoaded',(event)=>{
        const contactForm=document.getElementById("contact-form");
        
        contactForm.addEventListener("submit",function(event){
            event.preventDefault();
            change();
            var request=new XMLHttpRequest();
            var url="/contact";
            var name=document.getElementById("name");
            var email=document.getElementById("email");
            var phone=document.getElementById("phone");
            var message=document.getElementById("message");
            var data=JSON.stringify({"name":name.value,"email":email.value,"phone":phone.value,"message":message.value,"_token":"{{ csrf_token() }}"});
            request.open("POST",url,true);
            request.setRequestHeader("Content-Type","application/json");
            request.onreadystatechange=function(){
                if(request.readyState===4){
                    if(request.status===200){
                        var jsonData=JSON.parse(request.response);
                        console.log(jsonData);
                        name.value="";
                        email.value="";
                        phone.value="";
                        message.value="";
                        change();
                        alert("Thank you. We will get back to you soon.");
                    }else{
                    change();
                    alert("An error has occurred.");
                    }
                }
            };
            request.send(data);
        });
    });
    
    function change(){
        var elem=document.getElementById("submit_btn");
        if(elem.innerHTML=="Submit")elem.innerHTML="Sending...";else elem.innerHTML="Submit";
    }
    </script>
@endsection