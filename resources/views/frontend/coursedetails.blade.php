@extends('frontend_layouts.master')

@section('title', 'Courses | DSRTTRI')


@section('content')
<div class="kingster-page-wrapper" id="kingster-page-wrapper" style="padding-top:30px">
    <div class="container">
                    
    <div class="kingster-item-pdlr">
        <h4 style="margin-bottom:0;">Bachelor of Education</h4>
    </div>
    
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-white" style="padding-top:0;padding-left: 0;margin-left: 20px;">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Courses</a></li>
        <li class="breadcrumb-item active" aria-current="page">Bachelor of Education</li>
      </ol>
    </nav>
    <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
                    
    <div class=" kingster-sidebar-wrap clearfix kingster-line-height-0 kingster-sidebar-style-right">
                        
                        <div class=" kingster-sidebar-center kingster-column-40 kingster-line-height">
                            <div class="gdlr-core-page-builder-body">
                                <div class="gdlr-core-pbf-wrapper" style="padding-top:0">
                                    <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                                        <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                            
<div class="card" style="background-color:#ec2f45;color:#ffffff">
  <div class="card-body">
    <p class="card-text">
        <span class="col-6" style="padding-left:0"><b>Course Id:</b> B.Ed.</span>
        <span class="col-6" style="padding-left:0"><b>Duration:</b> 2 Years</span>
        <br/>
        <b>Eligibility:</b> Graduation from Recognized Board
        <br/>
        <b>Mode :</b> Full-time
        <br/>
        <b>Certification:</b> Gauhati University, Approved by Govt. of Assam
    </p>
  </div>
</div>

<div>
<div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-top: 40px ;">
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title text-danger" style="font-size: 20px ;font-weight: 600 ;">About the 2-Year B.Ed. Course</h3></div>
                                                </div>
                                            </div>
<div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
        <p class="">1. The structure of the present syllabus is relevant to face to face and regularmode of education only.
        <p>2. A Candidate may be allowed to appear in the Bachelor of Education (B. Ed.)Examination provided that after passing B.A/B.Sc./B.Com. or M.A.</p> M.Sc./M. Com. Examination of this University or of any other University,with at least 50% (45% for SC/ST/OBC) marks, recognized for this purpose,he/she has prosecuted for not less than 2 years, a regular course of study inthe subjects offered by him/her in a college affiliated to this University forthis purpose and has in addition, undergone a course of practical training as indicated below.</p>
</div>

<div>
<div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-top: 40px ;">
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title text-danger" style="font-size: 20px ;font-weight: 600 ;">Admission Procedure</h3></div>
                                                </div>
                                            </div>
<div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
        <p class="">Gauhati University invites application from interested candidate for entrance test conducted by the University for the B.Ed. course. After qualifying in the entrance test, a candidate is eligible for admission in Gauhati university affiliated to Dr. S R T T and RI and annual intake as 50 (Fifty)</p>
        <p>As per the approved regulations’ of Gauhati University a candidate possessing at least 50% marks in the aggregate at BA/B Se/B. com (10+2+3 pattern) or an MA/M.Sc/M.Com. degree of a recognized University is eligible for admission into the B.Ed. programme, provided the applicant has offered at least two school subjects at the first/or second year degree level. There is a relaxation of 5% marks for the candidates belonging to ST/SC/OBC/MOBC.

</p>
</div> 

<div>
<div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-top: 40px ;">
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title text-danger" style="font-size: 20px ;font-weight: 600 ;">Migration & Eligibility Criteria</h3></div>
                                                </div>
                                            </div>
<div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
        <p class="">Students from university other than Gauhati University shall have to produce Migration Certificate at the time of admission. They must also obtain Eligibility Certificate from the Gauhati University by paying required fees. At present eligibility and registration fee for students from the universities within the state are Rs. 1000 /- (Rupees One Thousand) only and Rs. 2000/- (Rupees Two Thousand) only respectively. For the students from the universities outside the state of Assam, the eligibility and registration fees are Rs. 7500 /- (Rupees Seven Thousand and Five Hundred) only and Rs. 2000 /- (Rupees Two Thousand) only respectively.</p>

        <p>Admission will be made through a common entrance examination conducted by Gauhati University.

    </p>
        
        <!-- <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-icon-list-item gdlr-core-item-pdlr gdlr-core-item-pdb clearfix " style="padding-bottom: 25px ;">
                                                    <ul>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Airline Management
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Airline flight operations
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Airline ground operations
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Airport management
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Airport operations
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Airport planning
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Aviation consultancy firms
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Aviation charter firms
                                                            </span></div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>-->

                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" id="div_1dd7_19">
                                                    <div class="gdlr-core-text-box-item-content" id="div_1dd7_20">
                                                        <div>
                                                        <img src="/frontend/images/course1.png" alt="Principal Image" style="float:left;padding: 0 1em 1em 0;"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

<div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-top: 40px ;">
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title text-danger" style="font-size: 20px ;font-weight: 600 ;">Examination Rules</h3></div>
                                                </div>
                                            </div>
<div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
        <p class=""><li>Marks of external & internal examination will not be shown separately.</li>
                    <li>A candidate, in order to pass, must obtain at least 40% marks in aggregate i.e. a total of 560 marks out of 1400 marks.</li>
                    <li>A candidate will have to secure at least 40 % marks in individual paper.</li>
                    <li>A candidate who secures below 40 % in one individual paper but if he/she secures aggregate 40% will be allowed to reappear in the examination as a back paper. (Maximum two back papers will be allowed)</li>
                    <li>Candidates securing at least 840 or 60% marks shall be declared to have obtained a First Class.</li>
                    <li>Candidates securing marks between 560 to 839 (both inclusive) shall be declared to have obtained a Second Class.</li>
                    <li>A candidate who fails to pass or present himself for the B.Ed.
                    Examination shall not be entitled to claim a refund of the examination fee nor will such fee be carried over for subsequent examination.</li>
                    <li>If a student after completion of a regular B.Ed course and after having obtained his/her admit card does not appear in the B.Ed Final examination for some unavoidable reason, he/she may appear in the next subsequent examination as a non-collegiate and will be required to pay the prescribed fees.</li>
                    <li>If a candidate</li>
                    <p>(I) fails in theory part only</p>
                    <p>(II) fails in Practical part only</p>
                    <p>(III) fails to secure pass marks in assignment paper (s)</p>
                    <p>But otherwise eligible (i.e. 40% out of 1400) to pass the B.Ed Examination will be given a chance to re-appear or submit the report within two years to clear his/her paper(s)/Practical/report/assignment. But the candidate must take casual admission(s) for the year(s) and pay all the prescribed fees for the subsequent examination(s). If a candidate fails in only one theory paper in the third year from the year of his/her admission he/she shall have a chance to reappear in that paper only in the immediate next year. Results of all such candidates will be declared as per clause 3. (b), 3. (e) & 3. (f)</p>
</div>

<div>



        
                </div> 

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class=" kingster-sidebar-right kingster-column-20 kingster-line-height kingster-line-height">
                            <div class="kingster-sidebar-area kingster-item-pdlr" style="padding-top:0">
                                
                               <!-- <div id="text-5" class="widget widget_text kingster-widget" style="background-color: #f7f7f7">
                                        <h3 class="kingster-widget-title">Other BBA Programs</h3><span class="clear"></span>
                                        <div class="textwidget">
                                            <ul>
                                                            <li>BBA in Hospitality Management</li>
                                                            <li>BBA in Tourism Management</li>
                                                            <li>BBA in Aviation, Hospitality & Tourism Management</li>
                                                        </ul>
                                        </div>
                                </div>-->
                                
                                
                                
                                <div id="text-18" class="widget widget_text kingster-widget">
                                    <div class="textwidget">
                                        <div class="gdlr-core-widget-box-shortcode " style="color: #ffffff ;padding: 30px 45px;background-color: #ec2f45 ;">
                                            <div class="gdlr-core-widget-box-shortcode-content">
                                                </p>
                                                <h3 style="font-size: 20px; color: #fff; margin-bottom: 25px;">Contact Info</h3>
                                                <p><span style="font-size: 15px;">Chamata
                                                    <br /> <br /> Nalbari - 781306</span></p>
                                                <p><span style="font-size: 15px;"><i class="fa fa-phone" aria-hidden="true"></i> (03624) 232000 <br /><i class="fa fa-envelope-o" aria-hidden="true"></i> info@dsrttri.com<br /> </span></p>
                                                <p><span style="font-size: 16px;"><i class="fa fa-clock-o" aria-hidden="true"></i> Mon &#8211; Sat 9:00A.M. &#8211; 5:00P.M.</span></p> <span class="gdlr-core-space-shortcode" style="margin-top: 40px ;"></span>
                                                
                                                <p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>      
                
    </div>
                    
</div>
@endsection