@extends('frontend_layouts.master')

@section('title', 'Facilities | DSRTTRI')


@section('content')
<div class="kingster-page-wrapper" id="kingster-page-wrapper" style="padding-top:30px">
    <div class="container">
                    
    <div class="kingster-item-pdlr">
        <h4 style="margin-bottom:0;">Facilities</h4>
    </div>
    
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-white" style="padding-top:0;padding-left: 0;margin-left: 20px;">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Facilities</li>
      </ol>
    </nav>
    <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
    <div class="row">
        <!--<div class="col-md-4">
            <img src="https://via.placeholder.com/450x360/000000/ffffff"></img>
            
            <div class="left-sub-box">
                
                <div class="kingster-sidebar-area kingster-item-pdlr" style="padding-top:0">
                                
                                <div id="text-5" class="widget widget_text kingster-widget" style="background-color: #f7f7f7">
                                        <h3 class="kingster-widget-title">Lorem ipsum</h3><span class="clear"></span>
                                        <div class="textwidget">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sed viverra ipsum nunc aliquet bibendum enim. In massa tempor nec feugiat. Nunc aliquet bibendum enim facilisis gravida. Nisl nunc mi ipsum faucibus vitae aliquet nec ullamcorper.</p>
                                        </div>
                                </div>
                </div>
                
                <div class="kingster-sidebar-area kingster-item-pdlr" style="padding-top:0">
                                
                                <div id="text-5" class="widget widget_text kingster-widget" style="background-color: #f7f7f7">
                                        <h3 class="kingster-widget-title">Lorem ipsum</h3><span class="clear"></span>
                                        <div class="textwidget">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sed viverra ipsum nunc aliquet bibendum enim. In massa tempor nec feugiat. Nunc aliquet bibendum enim facilisis gravida. Nisl nunc mi ipsum faucibus vitae aliquet nec ullamcorper.</p>
                                        </div>
                                </div>
                </div>
                
            </div>
            
            <div class="left-sub-box">
                <img src="https://via.placeholder.com/450x530/ec2f45/ffffff"></img>
            </div>
        </div>-->
        <div class="col-md-8" style="padding-bottom:2em">
            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-top: 40px ;">
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title text-danger" style="font-size: 20px ;font-weight: 600 ;">The Computer Lab</h3></div>
                                                </div>
                                            </div>
<div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
            <div>
                Dr. Sarvepolli Radhakrishnan Teacher's Training & Research Institute is equipped with laboratories, library, IT Laboratory and audio-visual facilities. The library has a collection of more than five thousand books on subjects related to the B.Ed. Curriculum. The college is a regular subscriber of education journals, periodicals, current affairs’ magazines and newspaper.
            </div>
            
            <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
                <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #000000; ;border-bottom-width: 1px ;"></div>
            </div>
            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-top: 40px ;">
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title text-danger" style="font-size: 20px ;font-weight: 600 ;">The Library</h3></div>
                                                </div>
                                            </div>
<div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
                        <div>
                5000 Books with journals, news paper, Reading Room Capacity for 50 students, Wifi facilities available
            </div>
            
            <!--<div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
                <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #000000; ;border-bottom-width: 1px ;"></div>
            </div>-->
            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-top: 40px ;">
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title text-danger" style="font-size: 20px ;font-weight: 600 ;">The Laboratory</h3></div>
                                                </div>
                                            </div>
<div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
                        <div>
                Separate labortory with apparatus like Psychology Lab, Art and Craft Lab, Physical Education Lab and ICT lab
            </div>
            
            
            
        </div>
    </div>

</div>
                    
</div>
@endsection