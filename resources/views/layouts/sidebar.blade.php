<!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <!--<li class="nav-item">
            <a href="/" class="nav-link {{ Request::is('/') ? 'active' : '' }}">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>-->
          <li class="nav-item">
            <a href="/admin/notice" class="nav-link {{ Request::is('admin/notice') ? 'active' : '' }}">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                Notices
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/admin/gallery" class="nav-link {{ Request::is('admin/gallery') ? 'active' : '' }}">
              <i class="nav-icon fas fa-images"></i>
              <p>
                Gallery
              </p>
            </a>
          </li>
          <!--<li class="nav-item">
            <a href="/allorders" class="nav-link {{ Request::is('allorders') ? 'active' : '' }}">
              <i class="nav-icon fas fa-chart-bar"></i>
              <p>
                All Orders
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/doctors" class="nav-link {{ Request::is('doctors') ? 'active' : '' }}">
              <i class="nav-icon fas fa-user-md"></i>
              <p>
                Doctors
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/revenue" class="nav-link {{ Request::is('revenue') ? 'active' : '' }}">
              <i class="nav-icon fas fa-chart-area"></i>
              <p>
                Revenue
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/offerimage" class="nav-link {{ Request::is('offerimage') ? 'active' : '' }}">
              <i class="fas fa-images"></i>
              <p>
                Offer Image
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/cost" class="nav-link {{ Request::is('cost') ? 'active' : '' }}">
              <i class="fas fa-dollar-sign"></i>
              <p>
                Services Cost
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/coupons" class="nav-link {{ Request::is('coupons') ? 'active' : '' }}">
              <i class="fas fa-tags"></i>
              <p>
                Coupons
              </p>
            </a>
          </li>-->
        </ul>
      </nav>
<!-- /.sidebar-menu -->