<!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <!--<div class="float-right d-none d-sm-inline">
      Anything you want
    </div>-->
    <!-- Default to the left -->
    <strong>Copyright &copy; {{ date("Y") }} <a target="_blank" href="https://xcraft.co">Xcraft Online Pvt Ltd</a>.</strong> All rights reserved.
  </footer>