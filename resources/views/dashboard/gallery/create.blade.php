@extends('layouts.master')

@section('page-title', 'Add New Gallery Image')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/admin/gallery">gallery</a></li>
    <li class="breadcrumb-item active">New</li>
</ol>
@endsection

@section('content')
<!-- Main content -->
  <section class="content">
    <div class="container-fluid">
        <div class="card">
            <!-- /.card-header -->
            <form class="form-horizontal" role="form" action="/admin/gallery" method="post" enctype="multipart/form-data">
                @csrf
            <div class="card-body col-md-9">
                <div class="form-group row">
				    <label class="col-sm-3 control-label">Title:</label>
				    <div class="col-sm-9">
					    <input type="text" class="form-control" name="title" value="{{old('title')}}">
				    </div>
			    </div>
			    <div class="form-group row">
				    <label class="col-sm-3 control-label">Description:</label>
				    <div class="col-sm-9">
					    <textarea class="form-control" rows="5" name="description">{!!old('description')!!}</textarea>
				    </div>
			    </div>
			    <div class="form-group row">
				    <label class="col-sm-3 control-label">Upload File:</label>
				    <div class="col-sm-9">
					    <input name="upload" type="file" class="form-control-file">
				    </div>
			    </div>
			    <div class="form-group row">
						    <div class="col-sm-offset-3 col-sm-9">
                                <div class="checkbox">
								<label>
								    <input type="checkbox" name="deactivate" value="1" @if(old('deactivate')) checked="checked" @endif> Deactivate
								</label>
								</div>
							</div>
						</div>
            </div>
            <div class="card-footer clearfix">
			    <button name="action" type="submit" value="SUBMIT" class="btn btn-primary">Submit</button>
              </div>
            </form>
        </div>
    </div>
  </section>
@endsection