@extends('layouts.master')

@section('page-title', 'Add New Notice')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/admin/notice">Notices</a></li>
    <li class="breadcrumb-item active">New</li>
</ol>
@endsection

@section('content')
<!-- Main content -->
  <section class="content">
    <div class="container-fluid">
        <div class="card">
            <!-- /.card-header -->
            <form class="form-horizontal" role="form" action="/admin/notice" method="post" enctype="multipart/form-data">
                @csrf
            <div class="card-body col-md-9">
                <div class="form-group row">
				    <label class="col-sm-3 control-label">Subject*:</label>
				    <div class="col-sm-9">
					    <input type="text" class="form-control" name="subject" value="{{old('subject')}}">
				    </div>
			    </div>
			    <div class="form-group row">
				    <label class="col-sm-3 control-label">Description*:</label>
				    <div class="col-sm-9">
					    <textarea class="form-control" rows="5" name="description">{!!old('description')!!}</textarea>
				    </div>
			    </div>
			    <div class="form-group row">
							<label class="col-sm-3 control-label">Category:</label>
							<div class="col-sm-9">
							<select name="category" class="form-control">
								<option value="general">General</option>
							    <option value="student">Student</option>
							    <option value="tender">Tender</option>
							    <option value="ncte">NCTE</option>
							    <option value="gu">G.U</option>
							</select>
							</div>
						</div>
			    <div class="form-group row">
				    <label class="col-sm-3 control-label">Upload File:</label>
				    <div class="col-sm-9">
					    <input name="upload" type="file" class="form-control-file">
				    </div>
			    </div>
			    <div class="form-group row">
						    <div class="col-sm-offset-3 col-sm-9">
                                <div class="checkbox">
								<label>
								    <input type="checkbox" name="deactivate" value="1" @if(old('deactivate')) checked="checked" @endif> Deactivate
								</label>
								</div>
							</div>
						</div>
            </div>
            <div class="card-footer clearfix">
			    <button name="action" type="submit" value="SUBMIT" class="btn btn-primary">Submit</button>
              </div>
            </form>
        </div>
    </div>
  </section>
@endsection