@extends('layouts.master')

@section('page-title', 'Notices')

@section('style')
  @parent
  <link rel="stylesheet" href="/vendor/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="/vendor/jquery-confirm/jquery-confirm.min.css">
  <style type="text/css">
      .hidecaret .dropdown-toggle::after {
          display: none;
        }
  </style>
@endsection

@section('content')
<!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                  <a href="/admin/notice/create" class="btn btn-primary" title="Add New Notice"><i class="fas fa-plus"></i> New Notice</a>
                <hr/>
                <div class="table-responsive">
                    <table id="mainTable" class="table table-bordered table-hover" cellspacing="0" width="100%">
                    	<thead>
                    		<tr>
                    			<th data-col="show">Sl</th>
                    			<th data-col="show">Subject</th>
                    			<th data-col="show">Category</th>
                    			<th data-col="show">Created At</th>
                    			<th data-col="show">Status</th>
                    			<th data-col="show">Action</th>
                    		</tr>
                    	</thead>
                    	<tbody>
                    @foreach ($notices as $key=>$notice)
                    		<tr>
                    			<td>{{ ($key+1) }}</td>
                    			<td>{{ $notice->subject }}</td>
                    			<td>{{ $notice->category }}</td>
                    			<td>{{ \Carbon\Carbon::parse($notice->created_at)->format('d-m-yy') }}</td>
                    			<td>{!! $notice->deactivate ? '<span class="badge badge-danger">Deactivated</span>' : '<span class="badge badge-primary">Active</span>' !!}</td>
                    			<td>
                    			    <div class="dropdown hidecaret">
                                        <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          <i class="fas fa-ellipsis-h"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="/admin/notice/{{$notice->id}}/edit"><i class="fas fa-edit"></i> Edit</a>
                                        </div>
                                      </div>
                    			</td>
                    		</tr>
                    @endforeach
                    	</tbody>
                    </table>
                    </div>
                </div>
        </div>
    </div>
  </section>
@endsection

@section('script')
	@parent
<script src="/vendor/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/vendor/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="/vendor/jquery-confirm/jquery-confirm.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		var table = $('#mainTable').DataTable({
		"aaSorting": [[ 0, "desc" ]],
	});
	});
</script>
@endsection