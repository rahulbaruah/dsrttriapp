@extends('layouts.master')

@section('page-title', 'Edit/Update Notice')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/admin/notice">Notices</a></li>
    <li class="breadcrumb-item active">Edit</li>
</ol>
@endsection

@section('content')
<!-- Main content -->
  <section class="content">
    <div class="container-fluid">
        <div class="card">
            <!-- /.card-header -->
            <form class="form-horizontal" role="form" action="/admin/notice/{{$notice->id}}" method="post" enctype="multipart/form-data">
                @method('PUT')
                @csrf
            <div class="card-body col-md-9">
                <div class="form-group row">
				    <label class="col-sm-3 control-label">Subject:</label>
				    <div class="col-sm-9">
					    <input type="text" class="form-control" name="subject" value="{{$notice->subject}}">
				    </div>
			    </div>
			    <div class="form-group row">
				    <label class="col-sm-3 control-label">Description:</label>
				    <div class="col-sm-9">
					    <textarea class="form-control" rows="5" name="description">{!!$notice->description!!}</textarea>
				    </div>
			    </div>
			    <div class="form-group row">
							<label class="col-sm-3 control-label">Category:</label>
							<div class="col-sm-9">
							<select name="category" class="form-control">
								<option value="general">General</option>
							    <option value="student">Student</option>
							    <option value="tender">Tender</option>
							    <option value="ncte">NCTE</option>
							    <option value="gu">G.U</option>
							</select>
							</div>
						</div>
				@section('script')
						    @parent
						<script type="text/javascript">
						    $(document).ready(function(){
						        @if($notice->category)
						       $('select[name="category"]').val("{{$notice->category}}");
						       @endif
						    });
						</script>
						@endsection
			    <div class="form-group row">
				    <label class="col-sm-3 control-label">Upload File:</label>
				    <div class="col-sm-9">
				        @if($notice->uploaded_file)
				        <a href="{{asset($notice->uploaded_file)}}" target="_blank">{{basename($notice->uploaded_file)}}</a>
				        @endif
					    <input name="upload" type="file" class="form-control-file">
				    </div>
			    </div>
			    <div class="form-group row">
						    <div class="col-sm-offset-3 col-sm-9">
                                <div class="checkbox">
								<label>
								    <input type="checkbox" name="deactivate" value="1" @if($notice->deactivate) checked="checked" @endif> Deactivate
								</label>
								</div>
							</div>
						</div>
            </div>
            <div class="card-footer clearfix">
			    <button name="action" type="submit" value="SUBMIT" class="btn btn-primary">Submit</button>
              </div>
            </form>
        </div>
    </div>
  </section>
@endsection