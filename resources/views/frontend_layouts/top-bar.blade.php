<div class="kingster-top-bar">
                <div class="kingster-top-bar-background"></div>
                <div class="kingster-top-bar-container kingster-container ">
                    <div class="kingster-top-bar-container-inner clearfix">
                        <div class="kingster-top-bar-left kingster-item-pdlr">
                            <i class="fa fa-phone" style="font-size: 15px ;margin-right: 6px ;"></i> (03624) 232000
                            <i class="fa fa-envelope" style="font-size: 15px ;margin-left: 18px ;margin-right: 6px ;"></i> info@dsrttri.com
                        </div>
                        <div class="kingster-top-bar-right kingster-item-pdlr">
                            <ul id="kingster-top-bar-menu" class="sf-menu kingster-top-bar-menu kingster-top-bar-right-menu">
                                
                                <li class="menu-item kingster-normal-menu"><a href="/notice/ncte">NCTE</a></li>
                                <li class="menu-item kingster-normal-menu"><a href="/notice/gu">G.U</a></li>
                                <li class="menu-item kingster-normal-menu"><a href="#">Download Brochure</a></li>
                                <!--<li class="menu-item kingster-normal-menu">
                                    <div class="input-group">
    <input type="text" class="form-control" placeholder="Search">
    <div class="input-group-append" id="search-btn-icon">
      <button class="btn btn-secondary" type="button">
        <i class="fa fa-search"></i>
      </button>
    </div>
  </div>
                                    
                                </li>-->
                            </ul>
                            <!--<div class="kingster-top-bar-right-social"></div><a class="kingster-top-bar-right-button" href="#" target="_blank">APPLY ONLINE</a>-->
                        </div>
                    </div>
                </div>
</div>