<footer>
                <div class="kingster-footer-wrapper ">
                    <div class="kingster-footer-container kingster-container clearfix">
                        <div class="kingster-footer-column kingster-item-pdlr kingster-column-15">
                            <div id="text-2" class="widget widget_text kingster-widget">
                                <div class="textwidget">
                                    <p style="background: #fff;padding: 5px;"><img src="/frontend/images/logo-wide1.png" alt="" />
                                        <!--<br /> <span class="gdlr-core-space-shortcode" style="margin-top: 5px ;"></span>-->
                                        
                                    <p><span style="font-size: 15px; color: #ffffff;">
                                        Chamata, Nalbari,
                                        <br /> Assam - 781 306
                                        <br/>
                                        <i class="fa fa-phone"></i> (03624) 232000</span>
                                        <br /> <span class="gdlr-core-space-shortcode" style="margin-top: -20px ;"></span>
                                        <br /> <a style="font-size: 15px; color: #ffffff;" href="mailto:principalamch@rediffmail.com"><i class="fa fa-envelope"></i> info@dsrttri.com</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="kingster-footer-column kingster-item-pdlr kingster-column-15">
                            <div id="gdlr-core-custom-menu-widget-2" class="widget widget_gdlr-core-custom-menu-widget kingster-widget">
                                <h3 class="kingster-widget-title">Quick Links</h3><span class="clear"></span>
                                <div class="menu-our-campus-container">
                                    <ul id="menu-our-campus" class="gdlr-core-custom-menu-widget gdlr-core-menu-style-plain">
                                        <li class="menu-item"><a href="#">Home</a></li>
                                        <li class="menu-item"><a href="#">About Us</a></li>
                                        <li class="menu-item"><a href="#">Audit Report</a></li>
                                        <li class="menu-item"><a href="#">Tenders</a></li>
                                        <li class="menu-item"><a href="#">Students</a></li>
                                        <li class="menu-item"><a href="#">Contact Us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="kingster-footer-column kingster-item-pdlr kingster-column-15">
                            <div id="gdlr-core-custom-menu-widget-3" class="widget widget_gdlr-core-custom-menu-widget kingster-widget">
                                <h3 class="kingster-widget-title">NCTE</h3><span class="clear"></span>
                                <div class="menu-campus-life-container">
                                    <ul id="menu-campus-life" class="gdlr-core-custom-menu-widget gdlr-core-menu-style-plain">
                                        <li class="menu-item"><a href="#">Annual Report</a></li>
                                        <li class="menu-item"><a href="#"></a>Balance Sheet</li>
                                        <li class="menu-item"><a href="#"></a>Misc. Reports</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="kingster-footer-column kingster-item-pdlr kingster-column-15">
                            <div id="gdlr-core-custom-menu-widget-4" class="widget widget_gdlr-core-custom-menu-widget kingster-widget">
                                <h3 class="kingster-widget-title">Quick Links</h3><span class="clear"></span>
                                <div class="menu-academics-container">
                                    <ul id="menu-academics" class="gdlr-core-custom-menu-widget gdlr-core-menu-style-plain">
                                        <li class="menu-item"><a href="#">Sitemap</a></li>
                                        <li class="menu-item"><a href="#">Results</a></li>
                                        <li class="menu-item"><a href="#">RTI</a></li>
                                        <li class="menu-item"><a href="#">Facilities</a></li>
                                        <li class="menu-item"><a href="#">Affiliation</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
				<div class="kingster-copyright-wrapper">
					<div class="kingster-copyright-container kingster-container clearfix">
						<div class="kingster-copyright-left kingster-item-pdlr">&copy; {{ date("Y") }}, DSRTTRI | All Right Reserved.</div>
						<div class="kingster-copyright-left">Website designed and maintained by <a href="//xcraft.co" target="_blank" title="Xcraft Online Pvt. Ltd.">Xcraft Online Pvt. Ltd.</a></div>
						<div class="kingster-copyright-right kingster-item-pdlr">
							<div class="gdlr-core-social-network-item gdlr-core-item-pdb  gdlr-core-none-align" style="padding-bottom: 0px ;">
								<a href="#" target="_blank" class="gdlr-core-social-network-icon" title="facebook">
									<i class="fa fa-facebook" ></i>
								</a>
								<a href="#" target="_blank" class="gdlr-core-social-network-icon" title="google-plus">
									<i class="fa fa-google-plus" ></i>
								</a>
								<a href="#" target="_blank" class="gdlr-core-social-network-icon" title="linkedin">
									<i class="fa fa-linkedin" ></i>
								</a>
								<a href="#" target="_blank" class="gdlr-core-social-network-icon" title="skype">
									<i class="fa fa-skype" ></i>
								</a>
								<a href="#" target="_blank" class="gdlr-core-social-network-icon" title="twitter">
									<i class="fa fa-twitter" ></i>
								</a>
								<a href="#" target="_blank" class="gdlr-core-social-network-icon" title="instagram">
									<i class="fa fa-instagram" ></i>
								</a>
							</div>
						</div>
					</div>
				</div>
            </footer>
