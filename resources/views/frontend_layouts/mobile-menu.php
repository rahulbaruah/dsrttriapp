<div class="kingster-mobile-menu"><a class="kingster-mm-menu-button kingster-mobile-menu-button kingster-mobile-button-hamburger" href="#kingster-mobile-menu"><span></span></a>
                        <div class="kingster-mm-menu-wrap kingster-navigation-font" id="kingster-mobile-menu" data-slide="right">
                            <ul id="menu-main-navigation" class="m-menu">
                                <li class="menu-item kingster-normal-menu"><a href="/">Home</a></li>
                                    
                                    <li class="menu-item kingster-normal-menu"><a href="/about">About Us</a></li>
                                    
                                    <li class="menu-item kingster-normal-menu"><a href="/coursedetails">Courses</a></li>
                                    
                                    <li class="menu-item kingster-normal-menu"><a href="/faculty">Faculty</a></li>
                                    
                                    <li class="menu-item kingster-normal-menu"><a href="/facilities">Facilities</a></li>
                                <li class="menu-item menu-item-home menu-item-has-children"><a href="#">Notices</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-home"><a href="/notice/general">General</a></li>
                                        <li class="menu-item"><a href="/notice/student">Student</a></li>
                                        <li class="menu-item"><a href="/notice/tender">Tender</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item"><a href="/gallery">Gallery</a></li>
                                <li class="menu-item"><a href="/contact">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>