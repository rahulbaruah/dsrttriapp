<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alkhachatryan\LaravelWebConsole\LaravelWebConsole;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('dashboard.index');
    }
    
    public function ShowTerminal() {
       return LaravelWebConsole::show();
    }
}
