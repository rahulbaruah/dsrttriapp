<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notice;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $generalnotices = Notice::where(function ($query) {
                                    $query->where('deactivate', 0)
                                          ->orWhere('deactivate', null);
                         })
                         ->where('category','general')
                         ->orderBy('created_at','desc')
                         ->take(3)
                        ->get();
        $studentnotices = Notice::where(function ($query) {
                                    $query->where('deactivate', 0)
                                          ->orWhere('deactivate', null);
                         })
                         ->where('category','student')
                         ->orderBy('created_at','desc')
                         ->take(3)
                        ->get();
        $tendernotices = Notice::where(function ($query) {
                                    $query->where('deactivate', 0)
                                          ->orWhere('deactivate', null);
                         })
                         ->where('category','tender')
                         ->orderBy('created_at','desc')
                         ->take(3)
                        ->get();
        $data = ['generalnotices' => $generalnotices,'studentnotices' => $studentnotices,'tendernotices' => $tendernotices];
        
        return view('frontend.index')->with($data);
    }
}
