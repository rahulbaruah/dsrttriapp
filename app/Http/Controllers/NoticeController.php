<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notice;
use Storage;

class NoticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notices = Notice::all();
        
        $data = ['notices' => $notices];
        return view('dashboard.notice.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.notice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'subject' => 'required',
			'description' => 'required',
		]);
		
		$notice = new Notice;
		$notice->subject = $request->input('subject');
		$notice->description = $request->input('description');
		$notice->category = $request->input('category');
		$notice->deactivate = $request->input('deactivate');
		$notice->save();
		
		if($request->hasFile('upload')) {
		    $file = $request->file('upload');
        		$path = $file->getRealPath();
                $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $extension = strtolower($file->getClientOriginalExtension()); // getting file extension
                $filesize = $file->getSize();
                $filesize = $filesize/1000000; /*-----MB------*/
                $fileName = 'notice_'.clean($originalName).'_'.time().'.'.$extension; // renaming file
                
                /*--------check filesize------------*/
                if ($filesize>10) {    /*---------3MB-------*/
                    $msg = ['Please provide a file of size less than 10MB'];
                    return redirect()->back()->with(['msg' => $msg]);
                }
                
                $file_path = 'notice/'.$notice->id.'/';
                Storage::disk('public')->put($file_path.$fileName, file_get_contents($path));
                $notice->uploaded_file = 'storage/'.$file_path.$fileName;
                $notice->save();
		    }
		
		$data = ["msg" => ["Notice created successfully"]];
		return redirect('admin/notice')->with($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notice = Notice::find($id);
		$data = ['notice' => $notice];
		
		return view('dashboard.notice.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'subject' => 'required',
			'description' => 'required',
		]);
		
		$notice = Notice::find($id);
		$notice->subject = $request->input('subject');
		$notice->description = $request->input('description');
		$notice->category = $request->input('category');
		$notice->deactivate = $request->input('deactivate');
		$notice->save();
		
		if($request->hasFile('upload')) {
		    $file = $request->file('upload');
        		$path = $file->getRealPath();
                $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $extension = strtolower($file->getClientOriginalExtension()); // getting file extension
                $filesize = $file->getSize();
                $filesize = $filesize/1000000; /*-----MB------*/
                $fileName = 'notice_'.clean($originalName).'_'.time().'.'.$extension; // renaming file
                
                /*--------check filesize------------*/
                if ($filesize>10) {    /*---------3MB-------*/
                    $msg = ['Please provide a file of size less than 10MB'];
                    return redirect()->back()->with(['msg' => $msg]);
                }
                
                $file_path = 'notice/'.$notice->id.'/';
                Storage::disk('public')->put($file_path.$fileName, file_get_contents($path));
                $notice->uploaded_file = 'storage/'.$file_path.$fileName;
                $notice->save();
		    }
		
		$data = ["msg" => ["Notice updated successfully"]];
		return redirect('admin/notice')->with($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function NoticePage($category){
        $notices = Notice::where(function ($query) {
                                    $query->where('deactivate', 0)
                                          ->orWhere('deactivate', null);
                         })
                         ->where('category',$category)
                         ->orderBy('created_at','desc')
                        ->get();
        $data = ['notices' => $notices, 'category' => $category];
        
        return view('frontend.notice')->with($data);
    }
    
    public function NoticeDetailPage($id) {
        $notice = Notice::where(function ($query) {
                                    $query->where('deactivate', 0)
                                          ->orWhere('deactivate', null);
                         })
                         ->whereId($id)
                        ->first();
        $data = ['notice' => $notice];
        
        return view('frontend.noticedetails')->with($data);
    }
}
