<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;
use Storage;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gallery = Gallery::all();
        
        $data = ['gallery' => $gallery];
        return view('dashboard.gallery.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'upload' => 'required',
		]);
		
		$gallery = new Gallery;
		$gallery->title = $request->input('title');
		$gallery->description = $request->input('description');
		$gallery->deactivate = $request->input('deactivate');
		$gallery->save();
		
		if($request->hasFile('upload')) {
		    $file = $request->file('upload');
		    
		    $data = getimagesize($file);
		    $width = $data[0];
            $height = $data[1];
            
        		$path = $file->getRealPath();
                $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $extension = strtolower($file->getClientOriginalExtension()); // getting file extension
                $filesize = $file->getSize();
                $filesize = $filesize/1000000; /*-----MB------*/
                $fileName = 'img_'.clean($originalName).'_'.time().'.'.$extension; // renaming file
                
                /*--------check filesize------------*/
                if ($filesize>3) {    /*---------3MB-------*/
                    $msg = ['Please provide a file of size less than 3MB'];
                    return redirect()->back()->with(['msg' => $msg]);
                }
                
                $file_path = 'gallery/'.$gallery->id.'/';
                Storage::disk('public')->put($file_path.$fileName, file_get_contents($path));
                $gallery->image_file = 'storage/'.$file_path.$fileName;
                $gallery->image_size = $width.'x'.$height;
                $gallery->save();
		    }
		
		$data = ["msg" => ["Image added to gallery successfully"]];
		return redirect('admin/gallery')->with($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gallery = Gallery::find($id);
		$data = ['gallery' => $gallery];
		
		return view('dashboard.gallery.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         /*$this->validate($request, [
			'upload' => 'required',
		]);*/
		
		$gallery = Gallery::find($id);
		$gallery->title = $request->input('title');
		$gallery->description = $request->input('description');
		$gallery->deactivate = $request->input('deactivate');
		$gallery->save();
		
		if($request->hasFile('upload')) {
		    $file = $request->file('upload');
		    
		    $data = getimagesize($file);
		    $width = $data[0];
            $height = $data[1];
		    
        		$path = $file->getRealPath();
                $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $extension = strtolower($file->getClientOriginalExtension()); // getting file extension
                $filesize = $file->getSize();
                $filesize = $filesize/1000000; /*-----MB------*/
                $fileName = 'img_'.clean($originalName).'_'.time().'.'.$extension; // renaming file
                
                /*--------check filesize------------*/
                if ($filesize>3) {    /*---------3MB-------*/
                    $msg = ['Please provide a file of size less than 3MB'];
                    return redirect()->back()->with(['msg' => $msg]);
                }
                
                $file_path = 'gallery/'.$gallery->id.'/';
                Storage::disk('public')->put($file_path.$fileName, file_get_contents($path));
                $gallery->image_file = 'storage/'.$file_path.$fileName;
                $gallery->image_size = $width.'x'.$height;
                $gallery->save();
		    }
		
		$data = ["msg" => ["Gallery image updated successfully"]];
		return redirect('admin/gallery')->with($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function GalleryPage(){
        $gallery = Gallery::where(function ($query) {
                                    $query->where('deactivate', 0)
                                          ->orWhere('deactivate', null);
                         })
                        ->orderBy('created_at','desc')
                        ->get();
        $data = ['gallery' => $gallery];
        
        return view('frontend.gallery')->with($data);
    }
}
