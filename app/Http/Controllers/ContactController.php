<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactUs;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function SendMail(Request $request){
        $data = [
            "name" => $request->input('name'),
            "email" => $request->input('email'),
            "phone" => $request->input('phone'),
            "message" => $request->input('message')
            ];
        Mail::to('info@dsrttri.com')->send(new ContactUs($data));
        
        return response()->json(['msg'=>'success']);
    }
}
