<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

/*Route::get('/', function(){
    return view('frontend.index');
});*/

Route::get('/', 'HomeController@index');

Route::get('/home', 'HomeController@index');

Route::get('about', function(){
    return view('frontend.about');
});
Route::get('coursedetails', function(){
    return view('frontend.coursedetails');
});
Route::get('faculty', function(){
    return view('frontend.faculty');
});
Route::get('facilities', function(){
    return view('frontend.facilities');
});
Route::get('contact', function(){
    return view('frontend.contact');
});
Route::get('/gallery', 'GalleryController@GalleryPage');
Route::get('/notice/{category}', 'NoticeController@NoticePage');
Route::get('/noticedetail/{id}', 'NoticeController@NoticeDetailPage');

Route::get('/ssh', 'AdminController@ShowTerminal');

Route::post('contact', 'ContactController@SendMail');

Route::prefix('admin')->group(function () {
    Route::get('/', 'AdminController@index');
    
    Route::get('/home', 'AdminController@index')->name('home');
    
    Route::group(['middleware' => ['auth']], function () {
        Route::resource('notice', 'NoticeController');
        
        Route::resource('gallery', 'GalleryController');
    });
});
